
module.exports = {
  extends: [
    './base',
    './rules/node',
  ].map(require.resolve),
};
