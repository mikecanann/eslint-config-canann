
module.exports = {
  extends: [
    './base',
    './rules/browser',
  ].map(require.resolve),
};
