# eslint-config-canann

Based on `eslint-config-airbnb`.

## Usage

1. Install dependencies:

  ```sh
  yarn add --dev \
    eslint \
    babel-eslint \
    eslint-plugin-import \
    eslint-plugin-flowtype \
    eslint-plugin-react \
    eslint-plugin-jsx-a11y
  ```

2. Install this package:

  ```sh
  yarn add --dev https://github.com/mikecanann/eslint-config-canann.git
  ```

3. Create the following `.eslintrc.json` file:

  ```json
  {
    "root": true,
    "extends": "canann"
  }
  ```

See [Airbnb's Javascript styleguide](https://github.com/airbnb/javascript) and
the [ESlint config docs](http://eslint.org/docs/user-guide/configuring#extending-configuration-files)
for more information.

## Improving this config

If you have any suggestions feel free to create a new issue or pull request.
Consider adding test cases if you're making complicated rules changes, like
anything involving regexes.

You can run tests with `yarn test`.

You can make sure this module lints with itself using `yarn run lint`.
