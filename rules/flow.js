
module.exports = {
  plugins: [
    'flowtype',
  ],
  extends: [
    'plugin:flowtype/recommended',
  ],
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: false,
    },
  },
  rules: {
    'flowtype/sort-keys': 'off',
    'flowtype/require-variable-type': 'off',
    'flowtype/delimiter-dangle': ['warn', 'always-multiline'],
    'flowtype/object-type-delimiter': ['error', 'comma'],
    'flowtype/no-dupe-keys': 'error',
    'flowtype/no-primitive-constructor-types': 'error',
    'flowtype/no-weak-types': ['warn', {
      any: true,
      Object: true,
      Function: true,
    }],
  },
};
