
module.exports = {
  env: {
    browser: true,
    'shared-node-browser': false,
  },
  rules: {
    'no-console': 'warn',
    'import/no-nodejs-modules': 'error',
  },
};
