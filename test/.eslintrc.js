
module.exports = {
  rules: {
    'no-shadow': 'off',
    'id-length': ['error', {
      min: 2,
      properties: 'never',
      exceptions: ['t'],
    }],
  }
}
