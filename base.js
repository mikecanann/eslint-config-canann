
module.exports = {
  extends: [
    './rules/best-practices',
    './rules/errors',
    './rules/strict',
    './rules/style',
    './rules/variables',
    './rules/es6',
    './rules/imports',
    './rules/flow',
    './rules/react',
    './rules/react-a11y',
  ].map(require.resolve),
};
